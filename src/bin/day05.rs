use rayon::prelude::*;
use std::str::FromStr;

use aoc_2023::*;

type Output1 = i64;
type Output2 = Output1;

#[derive(Debug, Clone, Copy)]
struct AlmanacRange {
    destination: i64,
    source: i64,
    length: i64,
}

impl FromStr for AlmanacRange {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let nums: Vec<i64> = parse_ws_separated(s);
        Ok(AlmanacRange {
            destination: nums[0],
            source: nums[1],
            length: nums[2],
        })
    }
}

impl AlmanacRange {
    fn upper_bound(&self) -> i64 {
        self.source + self.length
    }

    fn has(&self, key: i64) -> bool {
        key >= self.source && key < self.upper_bound()
    }

    fn map(&self, key: i64) -> Option<i64> {
        if !self.has(key) {
            return None;
        }

        Some(self.destination + (key - self.source))
    }

    // fn maps_to(&self) -> (i64, i64) {
    //     (self.destination, self.destination + self.length)
    // }

    // fn overlaps(&self, start: i64, length: i64) -> bool {
    //     (start < self.source || self.has(start))
    //         && (start + length >= self.upper_bound() || self.has(start + length))
    // }
}

struct Mapping {
    ranges: Vec<AlmanacRange>,
}

impl FromStr for Mapping {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lines = s.lines().skip(1);
        let ranges = lines.map(|l| l.parse().unwrap()).collect();
        Ok(Mapping { ranges })
    }
}

impl Mapping {
    fn map(&self, key: i64) -> i64 {
        self.ranges
            .iter()
            .find(|r| r.has(key))
            .and_then(|r| r.map(key))
            .unwrap_or(key)
    }
}

struct Day05 {
    seeds: Vec<i64>,
    mappings: Vec<Mapping>,
}
impl Solution<Output1, Output2> for Day05 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let lines = file_to_string(pathname);
        let mut segments = lines.split("\n\n");

        let seeds = parse_ws_separated(segments.next().unwrap().split(':').nth(1).unwrap());

        let mappings = segments.map(|m| m.parse().unwrap()).collect();

        Self { seeds, mappings }
    }

    fn part_1(&mut self) -> Output1 {
        self.seeds
            .iter()
            .map(|s| self.mappings.iter().fold(*s, |key, m| m.map(key)))
            .min()
            .unwrap()
    }

    fn part_2(&mut self) -> Output2 {
        // TODO: Optimize this part,
        //       even though it runs in 20s with rayon and release build
        let indices = (0..self.seeds.len())
            .map(|i| 2 * i)
            .take_while(|i| *i < self.seeds.len() - 1)
            .collect_vec();

        indices
            .par_iter()
            .flat_map(|i| {
                let start = self.seeds[*i];
                let length = self.seeds[i + 1];

                (start..start + length)
                    .into_par_iter()
                    .map(|j| self.mappings.iter().fold(j, |key, m| m.map(key)))
            })
            .min()
            .unwrap()
    }
}

fn main() -> Result<()> {
    Day05::main()
}

test_sample!(day_05, Day05, 35, 46);
