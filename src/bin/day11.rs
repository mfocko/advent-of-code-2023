use std::{
    cmp::{max, min},
    collections::HashSet,
};

use itertools::iproduct;

use aoc_2023::*;

type Output1 = usize;
type Output2 = Output1;

struct Day11 {
    galaxies: Vec<(usize, usize)>,
    empty_ys: HashSet<usize>,
    empty_xs: HashSet<usize>,
}

impl Day11 {
    fn count_within(xs: &HashSet<usize>, min_x: usize, max_x: usize) -> usize {
        xs.iter().filter(|&&x| min_x < x && x < max_x).count()
    }

    fn solve(&self, factor: usize) -> usize {
        let to_add = factor - 1;

        (0..self.galaxies.len() - 1)
            .map(|i| {
                let (y0, x0) = self.galaxies[i];

                (i + 1..self.galaxies.len())
                    .map(|j| {
                        let (y1, x1) = self.galaxies[j];

                        let mut xd = x0.abs_diff(x1);
                        let mut yd = y0.abs_diff(y1);

                        xd += to_add * Self::count_within(&self.empty_xs, min(x0, x1), max(x0, x1));
                        yd += to_add * Self::count_within(&self.empty_ys, y0, y1);

                        xd + yd
                    })
                    .sum::<usize>()
            })
            .sum()
    }
}

impl Solution<Output1, Output2> for Day11 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let lines: Vec<String> = file_to_lines(pathname);
        let map = lines.iter().map(|l| l.chars().collect_vec()).collect_vec();

        let galaxies = iproduct!((0..map.len()), (0..map[0].len()))
            .filter_map(|(y, x)| (map[y][x] != '.').then_some((y, x)))
            .collect();

        let empty_ys = map
            .iter()
            .enumerate()
            .filter(|&(_, l)| l.iter().all(|&c| c == '.'))
            .map(|(y, _)| y)
            .collect();

        let empty_xs = (0..map[0].len())
            .filter(|&x| (0..map[x].len()).all(|y| map[y][x] == '.'))
            .collect();

        Self {
            galaxies,
            empty_ys,
            empty_xs,
        }
    }

    fn part_1(&mut self) -> Output1 {
        self.solve(2)
    }

    fn part_2(&mut self) -> Output2 {
        self.solve(1000000)
    }
}

fn main() -> Result<()> {
    Day11::main()
}

test_sample!(day_11, Day11, 374, 82000210);

#[cfg(test)]
mod day_11_extended {
    use super::*;

    #[test]
    fn solve_for_10() {
        let path = Day11::get_sample(2);
        let day = Day11::new(path);
        assert_eq!(day.solve(10), 1030);
    }

    #[test]
    fn solve_for_100() {
        let path = Day11::get_sample(2);
        let day = Day11::new(path);
        assert_eq!(day.solve(100), 8410);
    }
}
