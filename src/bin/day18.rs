use std::{collections::BTreeMap, str::FromStr};

use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

use aoc_2023::*;

type Output1 = usize;
type Output2 = Output1;

struct Step {
    _direction: (isize, isize),
    count: isize,
    color: String,
}

impl FromStr for Step {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split_ascii_whitespace();

        let _direction = match parts.next().unwrap() {
            "U" => (0, -1),
            "D" => (0, 1),
            "L" => (-1, 0),
            "R" => (1, 0),
            _ => unreachable!(),
        };

        let count = parts.next().unwrap().parse().unwrap();
        let color = parts
            .next()
            .unwrap()
            .trim_matches(|c| c == '(' || c == ')')
            .to_owned();

        Ok(Step {
            _direction,
            count,
            color,
        })
    }
}

impl Step {
    fn direction(&self) -> Vector2D<isize> {
        Vector2D::new(self._direction.0, self._direction.1)
    }
}

fn dir_to_u8(u: (isize, isize)) -> u8 {
    match u {
        (0, 1) => 1,
        (0, -1) => 2,
        (1, 0) => 4,
        (-1, 0) => 8,
        _ => unreachable!(),
    }
}

fn opposite(u: (isize, isize)) -> (isize, isize) {
    (-u.0, -u.1)
}

fn solve(plan: &[Step]) -> usize {
    let mut hole: BTreeMap<isize, BTreeMap<isize, u8>> = BTreeMap::new();

    let mut position = Vector2D::new(0, 0);
    for s in plan.iter() {
        let direction = s.direction();

        for _ in 0..s.count {
            *(*hole.entry(position.y()).or_default())
                .entry(position.x())
                .or_default() |= dir_to_u8(s._direction);
            position = position + direction;
            *(*hole.entry(position.y()).or_default())
                .entry(position.x())
                .or_default() |= dir_to_u8(opposite(s._direction));
        }
    }

    hole.par_iter().map(|(_, row)| row.len()).sum::<usize>()
        + hole
            .par_iter()
            .map(|(_y, row)| {
                let mut in_between = 0;
                let mut x_prev = 0;

                let mut counter = 0;
                for (x, directions) in row.iter() {
                    if in_between != 0 {
                        let cells_between = (x - x_prev) as usize - 1;

                        counter += cells_between;
                    }
                    in_between ^= directions & 3;
                    x_prev = *x;
                }

                counter
            })
            .sum::<usize>()
}

struct Day18 {
    plan: Vec<Step>,
}
impl Solution<Output1, Output2> for Day18 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        Self {
            plan: file_to_structs(pathname),
        }
    }

    fn part_1(&mut self) -> Output1 {
        solve(&self.plan)
    }

    fn part_2(&mut self) -> Output2 {
        solve(
            &self
                .plan
                .iter()
                .map(|s| {
                    let new_step = s.color.trim_matches('#');
                    let number = isize::from_str_radix(new_step, 16).unwrap();

                    let _direction = match number % 16 {
                        0 => (1, 0),
                        1 => (0, 1),
                        2 => (-1, 0),
                        3 => (0, -1),
                        _ => unreachable!(),
                    };

                    Step {
                        _direction,
                        count: number >> 4,
                        color: String::new(),
                    }
                })
                .collect_vec(),
        )
    }
}

fn main() -> Result<()> {
    // Day18::run("sample")
    Day18::main()
}

test_sample!(day_18, Day18, 62, 952408144115);
