use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

#[derive(Debug)]
struct Race {
    time: i64,
    distance: i64,
}

impl Race {
    fn is_better(&self, t: i64) -> bool {
        t * (self.time - t) > self.distance
    }
}

struct Day06 {
    races: Vec<Race>,
}

impl Day06 {
    fn count_digits(mut n: i64, radix: i64) -> u32 {
        let mut digits = 0;

        while n > 0 {
            n /= radix;
            digits += 1;
        }

        digits
    }

    fn merge_races(&self) -> Race {
        self.races.iter().fold(
            Race {
                time: 0,
                distance: 0,
            },
            |mut m, r| {
                m.time *= 10_i64.pow(Self::count_digits(r.time, 10));
                m.distance *= 10_i64.pow(Self::count_digits(r.distance, 10));

                m.time += r.time;
                m.distance += r.distance;

                m
            },
        )
    }
}

impl Solution<Output1, Output2> for Day06 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let lines: Vec<String> = file_to_lines(pathname);

        let times: Vec<i64> = parse_ws_separated(lines[0].split(':').nth(1).unwrap());
        let distances: Vec<i64> = parse_ws_separated(lines[1].split(':').nth(1).unwrap());

        let races = times
            .iter()
            .zip(distances.iter())
            .map(|(&time, &distance)| Race { time, distance })
            .collect();

        Self { races }
    }

    fn part_1(&mut self) -> Output1 {
        self.races
            .iter()
            .map(|r| (1..r.time).filter(|t| r.is_better(*t)).count() as i32)
            .product()
    }

    fn part_2(&mut self) -> Output2 {
        let merged = self.merge_races();

        (1..merged.time).filter(|t| merged.is_better(*t)).count() as i32
    }
}

fn main() -> Result<()> {
    // Day06::run("sample")
    Day06::main()
}

test_sample!(day_06, Day06, 288, 71503);
