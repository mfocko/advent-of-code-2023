use std::{cmp, collections::HashMap};

use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

struct Number {
    y: usize,
    x_min: usize,
    x_max: usize,
    value: i32,
}

impl Number {
    fn new(y: usize, x_min: usize, x_max: usize, value: i32) -> Number {
        Number {
            y,
            x_min,
            x_max,
            value,
        }
    }
}

struct Day03 {
    symbols: HashMap<(usize, usize), char>,
    numbers: Vec<Number>,
}
impl Solution<Output1, Output2> for Day03 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let lines: Vec<String> = file_to_lines(pathname);

        let symbols = lines
            .iter()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars().enumerate().filter_map(move |(x, c)| {
                    if c != '.' && !c.is_ascii_digit() {
                        Some(((y + 1, x + 1), c))
                    } else {
                        None
                    }
                })
            })
            .collect::<HashMap<(usize, usize), char>>();

        let numbers = lines
            .iter()
            .enumerate()
            .flat_map(|(y, line)| {
                let mut nums = vec![];

                let mut start = line.len();
                let mut num = 0;
                for (x, c) in line.chars().enumerate() {
                    if c.is_ascii_digit() {
                        start = cmp::min(start, x);
                        num = num * 10 + c.to_digit(10).unwrap() as i32;
                    } else if start != line.len() {
                        nums.push(Number::new(y + 1, start + 1, x + 1, num));

                        start = line.len();
                        num = 0;
                    }
                }

                if start != line.len() {
                    nums.push(Number::new(y + 1, start + 1, line.len() + 1, num));
                }

                nums
            })
            .collect_vec();

        Self { symbols, numbers }
    }

    fn part_1(&mut self) -> Output1 {
        self.numbers
            .iter()
            .filter_map(|n| {
                if self.symbols.contains_key(&(n.y, n.x_min - 1))
                    || self.symbols.contains_key(&(n.y, n.x_max))
                    || (n.x_min - 1..=n.x_max).any(|xx| {
                        self.symbols.contains_key(&(n.y - 1, xx))
                            || self.symbols.contains_key(&(n.y + 1, xx))
                    })
                {
                    Some(n.value)
                } else {
                    None
                }
            })
            .sum()
    }

    fn part_2(&mut self) -> Output2 {
        let mut symbol_counters: HashMap<(usize, usize), Vec<i32>> = self
            .symbols
            .iter()
            .filter_map(|((y, x), s)| {
                if *s == '*' {
                    Some(((*y, *x), vec![]))
                } else {
                    None
                }
            })
            .collect();

        self.numbers.iter().for_each(|n| {
            let mut possible_gears = vec![(n.y, n.x_min - 1), (n.y, n.x_max)];

            (n.x_min - 1..=n.x_max).for_each(|xx: usize| {
                possible_gears.push((n.y - 1, xx));
                possible_gears.push((n.y + 1, xx));
            });

            for (y, x) in possible_gears {
                if let Some(nums) = symbol_counters.get_mut(&(y, x)) {
                    nums.push(n.value);
                }
            }
        });

        symbol_counters
            .values()
            .filter_map(|nums| {
                if nums.len() == 2 {
                    Some(nums.iter().product1::<i32>().unwrap())
                } else {
                    None
                }
            })
            .sum()
    }
}

fn main() -> Result<()> {
    Day03::main()
}

test_sample!(day_03, Day03, 4361, 467835);
