use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

struct Day09 {
    values: Vec<Vec<i32>>,
}

impl Day09 {
    fn find_next(mut xs: Vec<i32>) -> i32 {
        let last = *xs.last().unwrap();

        let mut ds = vec![];
        while xs.iter().any(|&x| x != 0) {
            for i in 0..xs.len() - 1 {
                xs[i] = xs[i + 1] - xs[i];
            }
            xs.pop();

            ds.push(*xs.last().unwrap());
        }

        last + ds.iter().rev().sum::<i32>()
    }
}

impl Solution<Output1, Output2> for Day09 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let values = file_to_lines::<Vec<_>, P>(pathname)
            .iter()
            .map(|vals| parse_ws_separated(vals))
            .collect_vec();

        Self { values }
    }

    fn part_1(&mut self) -> Output1 {
        self.values.iter().cloned().map(Self::find_next).sum()
    }

    fn part_2(&mut self) -> Output2 {
        self.values
            .iter()
            .map(|xs| {
                let mut ys = xs.clone();
                ys.reverse();

                ys
            })
            .map(Self::find_next)
            .sum()
    }
}

fn main() -> Result<()> {
    Day09::main()
}

test_sample!(day_09, Day09, 114, 2);
