use std::cmp::min;

use aoc_2023::*;
use itertools::iproduct;

type Output1 = i32;
type Output2 = Output1;

struct Pattern {
    map: Vec<Vec<char>>,
}

impl From<&[String]> for Pattern {
    fn from(value: &[String]) -> Self {
        Self {
            map: value.iter().map(|l| l.chars().collect_vec()).collect_vec(),
        }
    }
}

impl Pattern {
    fn transpose(&self) -> Pattern {
        let (m, n) = (self.map.len(), self.map[0].len());
        Pattern {
            map: (0..n)
                .map(|x| (0..m).map(|y| self.map[y][x]).collect_vec())
                .collect_vec(),
        }
    }

    fn check_horizontally(&self, allowed_error: usize) -> Option<i32> {
        (0..self.map.len() - 1)
            .find(|&y| {
                let d = min(y + 1, self.map.len() - y - 1);

                iproduct!(0..self.map[y].len(), (1..=d))
                    .filter(|&(x, dy)| self.map[y + 1 - dy][x] != self.map[y + dy][x])
                    .count()
                    == allowed_error
            })
            .map(|y| (y + 1) as i32)
    }

    fn score(&self, allowed_error: usize) -> i32 {
        if let Some(s) = self.transpose().check_horizontally(allowed_error) {
            s
        } else if let Some(s) = self.check_horizontally(allowed_error) {
            100 * s
        } else {
            unreachable!()
        }
    }
}

struct Day13 {
    patterns: Vec<Pattern>,
}
impl Solution<Output1, Output2> for Day13 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let lines: Vec<String> = file_to_lines(pathname);

        let patterns = lines
            .split(|l| l.is_empty())
            .map(|m| m.into())
            .collect_vec();

        Self { patterns }
    }

    fn part_1(&mut self) -> Output1 {
        self.patterns.iter().map(|p| p.score(0)).sum()
    }

    fn part_2(&mut self) -> Output2 {
        self.patterns.iter().map(|p| p.score(1)).sum()
    }
}

fn main() -> Result<()> {
    // Day13::run("sample")
    Day13::main()
}

test_sample!(day_13, Day13, 405, 400);
