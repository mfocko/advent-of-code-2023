use std::str::FromStr;

use indicatif::ParallelProgressIterator;
use memoize::memoize;
use rayon::prelude::*;

use aoc_2023::*;

type Output1 = usize;
type Output2 = Output1;

#[derive(Debug, PartialEq, Eq)]
struct Row {
    map: Vec<char>,
    damaged: Vec<usize>,
}

impl FromStr for Row {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (map, damaged) = s.split_once(' ').unwrap();

        let map = map.chars().collect();
        let damaged = parse_separated(',', damaged);

        Ok(Row::new(map, damaged))
    }
}

#[memoize]
fn arrangements(map: Vec<char>, damaged: Vec<usize>, in_damaged: bool) -> usize {
    // all damaged have been placed
    if damaged.is_empty() {
        return if map.iter().all(|&c| c == '.' || c == '?') {
            1
        } else {
            0
        };
    }

    let mut map = map.clone();
    let mut damaged = damaged.clone();
    let mut in_damaged = in_damaged;

    // skip the functional
    while let Some('.') = map.last() {
        if in_damaged {
            return 0;
        }

        in_damaged = false;
        map.pop();
    }

    if map.is_empty() || damaged.is_empty() {
        return 0;
    }

    let last_idx = map.len() - 1;
    let last_d_idx = damaged.len() - 1;

    match &map.last().unwrap() {
        '?' => {
            map[last_idx] = '.';
            let as_functional = if !in_damaged {
                arrangements(map.clone(), damaged.clone(), false)
            } else {
                0
            };

            map[last_idx] = '#';
            let as_non_functional = arrangements(map, damaged, true);

            as_functional + as_non_functional
        }
        '#' if damaged.is_empty() => 0,
        '#' => {
            *damaged.last_mut().unwrap() -= 1;

            map.pop();

            if damaged[last_d_idx] == 0 {
                damaged.pop();

                if let Some(&'#') = map.last() {
                    0
                } else if !map.is_empty() {
                    map[last_idx - 1] = '.';
                    arrangements(map, damaged, false)
                } else {
                    map.pop();
                    arrangements(map, damaged, false)
                }
            } else {
                arrangements(map, damaged, true)
            }
        }
        _ => unreachable!(),
    }
}

impl Row {
    fn new(map: Vec<char>, damaged: Vec<usize>) -> Row {
        Row { map, damaged }
    }

    fn arrangements(&mut self) -> usize {
        arrangements(self.map.clone(), self.damaged.clone(), false)
    }

    fn unfold(&self) -> Row {
        Row::new(
            self.map
                .iter()
                .cloned()
                .chain(['?'])
                .cycle()
                .take(5 * (self.map.len() + 1) - 1)
                .collect_vec(),
            self.damaged
                .iter()
                .cloned()
                .cycle()
                .take(5 * self.damaged.len())
                .collect_vec(),
        )
    }
}

struct Day12 {
    rows: Vec<Row>,
}
impl Solution<Output1, Output2> for Day12 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        Self {
            rows: file_to_structs(pathname),
        }
    }

    fn part_1(&mut self) -> Output1 {
        self.rows.iter_mut().map(|r| r.arrangements()).sum()
    }

    fn part_2(&mut self) -> Output2 {
        self.rows
            .par_iter()
            .progress()
            .map(|r| r.unfold().arrangements())
            .sum()
    }
}

fn main() -> Result<()> {
    // Day12::run("sample")
    Day12::main()
}

test_sample!(day_12, Day12, 21, 525152);

#[cfg(test)]
mod day_12_extended {
    use super::*;

    #[test]
    fn unfold_1() {
        let row = Row::new(".#".chars().collect_vec(), vec![1]);
        assert_eq!(
            row.unfold(),
            Row::new(".#?.#?.#?.#?.#".chars().collect_vec(), vec![1, 1, 1, 1, 1])
        );
    }

    #[test]
    fn unfold_2() {
        let row = Row::new("???.###".chars().collect_vec(), vec![1, 1, 3]);
        assert_eq!(
            row.unfold(),
            Row::new(
                "???.###????.###????.###????.###????.###"
                    .chars()
                    .collect_vec(),
                vec![1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3]
            )
        );
    }

    #[test]
    fn arrangements_1() {
        let mut row = Row::new("???.###".chars().collect_vec(), vec![1, 1, 3]);

        assert_eq!(row.arrangements(), 1);
        assert_eq!(row.unfold().arrangements(), 1);
    }

    #[test]
    fn arrangements_2() {
        let mut row = Row::new(".??..??...?##.".chars().collect_vec(), vec![1, 1, 3]);

        assert_eq!(row.arrangements(), 4);
        assert_eq!(row.unfold().arrangements(), 16384);
    }

    #[test]
    fn arrangements_3() {
        let mut row = Row::new("?#?#?#?#?#?#?#?".chars().collect_vec(), vec![1, 3, 1, 6]);

        assert_eq!(row.arrangements(), 1);
        assert_eq!(row.unfold().arrangements(), 1);
    }

    #[test]
    fn arrangements_4() {
        let mut row = Row::new("????.#...#...".chars().collect_vec(), vec![4, 1, 1]);

        assert_eq!(row.arrangements(), 1);
        assert_eq!(row.unfold().arrangements(), 16);
    }

    #[test]
    fn arrangements_5() {
        let mut row = Row::new("????.######..#####.".chars().collect_vec(), vec![1, 6, 5]);

        assert_eq!(row.arrangements(), 4);
        assert_eq!(row.unfold().arrangements(), 2500);
    }

    #[test]
    fn arrangements_6() {
        let mut row = Row::new("?###????????".chars().collect_vec(), vec![3, 2, 1]);

        assert_eq!(row.arrangements(), 10);
        assert_eq!(row.unfold().arrangements(), 506250);
    }

    #[test]
    fn arrangements_7() {
        let mut row = Row::new("????????????".chars().collect_vec(), vec![3, 2, 1]);

        assert_eq!(row.arrangements(), 35);
    }

    #[test]
    fn arrangements_8() {
        let mut row = Row::new("???#?#?????.?##??#".chars().collect_vec(), vec![8, 6]);

        assert_eq!(row.arrangements(), 4);
    }
}
