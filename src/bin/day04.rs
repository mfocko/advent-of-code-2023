use std::{collections::HashSet, str::FromStr};

use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

struct Card {
    winning: HashSet<i32>,
    draw: Vec<i32>,
}

impl FromStr for Card {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(':').nth(1).unwrap().split('|');
        let (winning, draw) = (parts.next().unwrap(), parts.next().unwrap());

        let winning: HashSet<i32> = parse_ws_separated(winning);
        let draw: Vec<i32> = parse_ws_separated(draw);

        Ok(Card { winning, draw })
    }
}

impl Card {
    fn matching(&self) -> usize {
        self.draw
            .iter()
            .filter(|card| self.winning.contains(card))
            .count()
    }

    fn score(&self) -> i32 {
        let mut matched = 0;

        for card in &self.draw {
            if self.winning.contains(card) {
                matched += 1;
            }
        }

        if matched > 1 {
            1 << (matched - 1)
        } else {
            matched
        }
    }
}

struct Day04 {
    cards: Vec<Card>,
}
impl Solution<Output1, Output2> for Day04 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let cards = file_to_structs(pathname);

        Self { cards }
    }

    fn part_1(&mut self) -> Output1 {
        self.cards.iter().map(Card::score).sum()
    }

    fn part_2(&mut self) -> Output2 {
        let mut counts = vec![1; self.cards.len()];

        for (i, card) in self.cards.iter().enumerate() {
            for j in 1..=card.matching() {
                counts[i + j] += counts[i];
            }
        }

        counts.iter().sum()
    }
}

fn main() -> Result<()> {
    Day04::main()
}

test_sample!(day_04, Day04, 13, 30);
