use std::{
    cmp::{self, Reverse},
    str::FromStr,
};

use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

lazy_static! {
    static ref CARDS_1: Vec<char> =
        vec!['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A',];
    static ref CARDS_2: Vec<char> =
        vec!['J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A',];
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Hand {
    cards: Vec<char>,
    bid: i32,
}

impl FromStr for Hand {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts = s.split_ascii_whitespace().collect_vec();

        let cards = parts[0].chars().collect_vec();
        let bid = parts[1].parse().unwrap();

        Ok(Self { cards, bid })
    }
}

impl Hand {
    fn make_comparator<'a>(
        card_ordering: &'a [char],
        hand_type_getter: &'a dyn Fn(&Self) -> HandType,
    ) -> impl Fn(&Hand, &Hand) -> cmp::Ordering + 'a {
        |x: &Self, y: &Self| {
            let my_type = hand_type_getter(x);
            let other_type = hand_type_getter(y);

            match my_type.partial_cmp(&other_type) {
                Some(cmp::Ordering::Equal) => {}
                Some(ord) => return ord,
                _ => {}
            }

            for (my, other) in x.cards.iter().zip(y.cards.iter()) {
                let l = card_ordering
                    .iter()
                    .enumerate()
                    .find_map(|(i, c)| if c == my { Some(i) } else { None })
                    .unwrap();
                let r = card_ordering
                    .iter()
                    .enumerate()
                    .find_map(|(i, c)| if c == other { Some(i) } else { None })
                    .unwrap();

                match l.partial_cmp(&r) {
                    Some(cmp::Ordering::Equal) => {}
                    Some(ord) => return ord,
                    _ => {}
                }
            }

            cmp::Ordering::Equal
        }
    }

    fn hand_type_no_joker(&self) -> HandType {
        let freqs = self.cards.iter().counts();
        let mut freqs = freqs.values().cloned().collect_vec();
        freqs.sort_by_key(|&c| Reverse(c));

        match freqs[..] {
            [5, ..] => HandType::FiveOfAKind,
            [4, ..] => HandType::FourOfAKind,
            [3, 2, ..] => HandType::FullHouse,
            [3, ..] => HandType::ThreeOfAKind,
            [2, 2] => HandType::TwoPair,
            [2, ..] => HandType::OnePair,
            _ => HandType::HighCard,
        }
    }

    fn hand_type_with_joker(&self) -> HandType {
        let mut freqs = self.cards.iter().counts();
        let jokers = *freqs.get(&'J').unwrap_or(&0);

        freqs.remove(&'J');
        let mut freqs = freqs.values().cloned().collect_vec();
        freqs.sort_by_key(|&c| Reverse(c));

        if jokers == 5 {
            return HandType::FiveOfAKind;
        }

        match freqs[..] {
            [n, ..] if n + jokers == 5 => HandType::FiveOfAKind,
            [n, ..] if n + jokers == 4 => HandType::FourOfAKind,
            [n, 2, ..] if n + jokers == 3 => HandType::FullHouse,
            [n, ..] if n + jokers == 3 => HandType::ThreeOfAKind,
            [2, 2, ..] => HandType::TwoPair,
            [n, ..] if n + jokers == 2 => HandType::OnePair,
            _ => HandType::HighCard,
        }
    }
}

struct Day07 {
    hands: Vec<Hand>,
}

impl Day07 {
    fn solve(
        &mut self,
        card_ordering: &[char],
        hand_type_getter: &dyn Fn(&Hand) -> HandType,
    ) -> i32 {
        self.hands
            .sort_by(Hand::make_comparator(card_ordering, hand_type_getter));
        self.hands
            .iter()
            .enumerate()
            .map(|(r, h)| (r as i32 + 1) * h.bid)
            .sum()
    }
}

impl Solution<Output1, Output2> for Day07 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let hands = file_to_structs(pathname);

        Self { hands }
    }

    fn part_1(&mut self) -> Output1 {
        self.solve(&CARDS_1, &Hand::hand_type_no_joker)
    }

    fn part_2(&mut self) -> Output2 {
        self.solve(&CARDS_2, &Hand::hand_type_with_joker)
    }
}

fn main() -> Result<()> {
    Day07::main()
}

test_sample!(day_07, Day07, 6440, 5905);
