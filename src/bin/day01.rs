use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

const NUMBERS: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

struct Day01 {
    lines: Vec<String>,
}
impl Solution<Output1, Output2> for Day01 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        Self {
            lines: file_to_lines(pathname),
        }
    }

    fn part_1(&mut self) -> Output1 {
        self.lines
            .iter()
            .map(|line| {
                let digits = line.chars().filter_map(|c| c.to_digit(10)).collect_vec();

                (10 * digits.first().unwrap() + digits.last().unwrap()) as i32
            })
            .sum()
    }

    fn part_2(&mut self) -> Output2 {
        self.lines
            .iter()
            .map(|line| {
                let mut digits: Vec<i32> = vec![];

                for (i, c) in line.chars().enumerate() {
                    if let Some(d) = c.to_digit(10) {
                        digits.push(d as i32);
                    }

                    if let Some(d) = NUMBERS.iter().enumerate().find_map(|(n, nstr)| {
                        if line[i..].starts_with(nstr) {
                            Some(n as i32 + 1)
                        } else {
                            None
                        }
                    }) {
                        digits.push(d)
                    }
                }

                10 * digits.first().unwrap() + digits.last().unwrap()
            })
            .sum()
    }
}

fn main() -> Result<()> {
    Day01::main()
}

test_sample!(day_01, Day01, 142, 281);
