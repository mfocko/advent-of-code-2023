use std::{
    collections::{HashMap, VecDeque},
    fmt::Display,
    fmt::Write,
};

use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn direction_vector(&self) -> Vector2D<isize> {
        match self {
            Direction::North => Vector2D::new(0, -1),
            Direction::East => Vector2D::new(1, 0),
            Direction::South => Vector2D::new(0, 1),
            Direction::West => Vector2D::new(-1, 0),
        }
    }

    fn state<'a>(
        &self,
        h: &'a mut HashMap<Direction, bool>,
        v: &'a mut HashMap<Direction, bool>,
    ) -> &'a mut HashMap<Direction, bool> {
        match self {
            Direction::North | Direction::South => v,
            Direction::East | Direction::West => h,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Vertical,
    Horizontal,
    NorthEast,
    NorthWest,
    SouthWest,
    SouthEast,
    Ground,
    Start,
}

impl Tile {
    fn directions(&self) -> Vec<Direction> {
        match self {
            Tile::Vertical => vec![Direction::North, Direction::South],
            Tile::Horizontal => vec![Direction::East, Direction::West],
            Tile::NorthEast => vec![Direction::North, Direction::East],
            Tile::NorthWest => vec![Direction::North, Direction::West],
            Tile::SouthWest => vec![Direction::South, Direction::West],
            Tile::SouthEast => vec![Direction::South, Direction::East],
            Tile::Ground => vec![],
            Tile::Start => vec![
                Direction::North,
                Direction::East,
                Direction::South,
                Direction::West,
            ],
        }
    }

    fn next(&self, position: Vector2D<isize>) -> Vec<Vector2D<isize>> {
        self.directions()
            .iter()
            .map(|d| d.direction_vector() + position)
            .collect_vec()
    }
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            '|' => Self::Vertical,
            '-' => Self::Horizontal,
            'L' => Self::NorthEast,
            'J' => Self::NorthWest,
            '7' => Self::SouthWest,
            'F' => Self::SouthEast,
            '.' => Self::Ground,
            'S' => Self::Start,
            _ => unreachable!("{} is an invalid tile", value),
        }
    }
}

impl Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(match self {
            Tile::Vertical => '║',
            Tile::Horizontal => '═',
            Tile::NorthEast => '╚',
            Tile::NorthWest => '╝',
            Tile::SouthWest => '╗',
            Tile::SouthEast => '╔',
            Tile::Ground => ' ',
            Tile::Start => 'S',
        })
    }
}

impl From<Tile> for char {
    fn from(value: Tile) -> Self {
        match value {
            Tile::Vertical => '|',
            Tile::Horizontal => '-',
            Tile::NorthEast => 'L',
            Tile::NorthWest => 'J',
            Tile::SouthWest => '7',
            Tile::SouthEast => 'F',
            Tile::Ground => '.',
            Tile::Start => 'S',
        }
    }
}

struct TileMap<'a>(&'a [Vec<Tile>]);
impl Display for TileMap<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.0 {
            for tile in row {
                f.write_fmt(format_args!("{}", tile))?;
            }
            f.write_char('\n')?;
        }

        Ok(())
    }
}

struct Day10 {
    pipes: Vec<Vec<Tile>>,
    distances: Vec<Vec<i32>>,
}

impl Day10 {
    fn find_start(&self) -> Vector2D<isize> {
        for y in 0..self.pipes.len() {
            for x in 0..self.pipes[y].len() {
                if self.pipes[y][x] == Tile::Start {
                    return Vector2D::new(x.try_into().unwrap(), y.try_into().unwrap());
                }
            }
        }

        unreachable!("there's always start")
    }

    fn bfs(&mut self) {
        let mut distances = vec![vec![-1; self.pipes[0].len()]; self.pipes.len()];

        // find the start and set the initial distance to zero
        let start = self.find_start();
        distances[start] = 0;

        let mut q = VecDeque::from([start]);
        while let Some(p) = q.pop_front() {
            for neighbour in self.pipes[p].next(p) {
                if in_range(&distances, &neighbour)
                    && distances[neighbour] == -1
                    && self.pipes[neighbour].next(neighbour).contains(&p)
                {
                    distances[neighbour] = distances[p] + 1;
                    q.push_back(neighbour);
                }
            }
        }

        self.distances = distances;

        // debug!("{:?}", distances);
    }
}

impl Solution<Output1, Output2> for Day10 {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        Self {
            pipes: file_to_string(pathname)
                .lines()
                .map(|s| s.chars().map(|c| c.into()).collect_vec())
                .collect_vec(),
            distances: vec![vec![]],
        }
    }

    fn part_1(&mut self) -> Output1 {
        self.bfs();

        self.distances
            .iter()
            .map(|row| *row.iter().max().unwrap())
            .max()
            .unwrap()
    }

    fn part_2(&mut self) -> Output2 {
        self.bfs();

        let mut counter = 0;

        let mut horizontal_enclosure: Vec<HashMap<Direction, bool>> =
            vec![
                HashMap::from([(Direction::West, false), (Direction::East, false)]);
                self.pipes[0].len()
            ];

        for (y, row) in self.pipes.iter().enumerate() {
            let mut vertical_enclosure: HashMap<Direction, bool> =
                HashMap::from([(Direction::North, false), (Direction::South, false)]);

            for (x, &t) in row.iter().enumerate() {
                if self.distances[y][x] != -1 {
                    for direction in t.directions() {
                        let directed_state: &mut HashMap<Direction, bool> =
                            direction.state(&mut horizontal_enclosure[x], &mut vertical_enclosure);

                        if let Some(val) = directed_state.get_mut(&direction) {
                            *val = !*val;
                        }
                    }
                } else if vertical_enclosure.values().any(|x| *x)
                    && horizontal_enclosure[x].values().any(|x| *x)
                {
                    counter += 1;
                }
            }
        }

        counter
    }
}

fn main() -> Result<()> {
    Day10::main()
}

test_sample!(day_10, Day10, 8, 10);
