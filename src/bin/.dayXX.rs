use aoc_2023::*;

type Output1 = i32;
type Output2 = Output1;

struct DayXX {}
impl Solution<Output1, Output2> for DayXX {
    fn new<P: AsRef<Path>>(pathname: P) -> Self {
        let lines: Vec<String> = file_to_lines(pathname);

        todo!()
    }

    fn part_1(&mut self) -> Output1 {
        todo!()
    }

    fn part_2(&mut self) -> Output2 {
        todo!()
    }
}

fn main() -> Result<()> {
    DayXX::main()
}

test_sample!(day_XX, DayXX, 0, 0);
