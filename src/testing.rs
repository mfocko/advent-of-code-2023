//! # Testing-related utilities

#[macro_export]
macro_rules! test_sample {
    ($mod_name:ident, $day_struct:tt, $part_1:expr, $part_2:expr) => {
        #[cfg(test)]
        mod $mod_name {
            use super::*;

            #[test]
            fn part_1() {
                let path = $day_struct::get_sample(1);
                let mut day = $day_struct::new(path);
                assert_eq!(day.part_1(), $part_1);
            }

            #[test]
            fn part_2() {
                let path = $day_struct::get_sample(2);
                let mut day = $day_struct::new(path);
                assert_eq!(day.part_2(), $part_2);
            }
        }
    };
}
