use std::ops::{Div, Mul, Rem};

pub fn gcd<T>(mut x: T, mut y: T) -> T
where
    T: Copy + Default + PartialEq + Rem<Output = T>,
{
    while y != T::default() {
        (x, y) = (y, x % y);
    }

    x
}

pub fn lcm<T>(x: T, y: T) -> T
where
    T: Copy + Default + PartialEq + Div<Output = T> + Mul<Output = T> + Rem<Output = T>,
{
    x / gcd(x, y) * y
}
