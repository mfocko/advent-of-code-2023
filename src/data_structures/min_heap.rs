use std::cmp::{Ord, Reverse};
use std::collections::BinaryHeap;

pub struct MinHeap<T> {
    heap: BinaryHeap<Reverse<T>>,
}

impl<T> MinHeap<T>
where
    T: Ord,
{
    pub fn new() -> Self {
        MinHeap {
            heap: BinaryHeap::new(),
        }
    }

    pub fn push(&mut self, item: T) {
        self.heap.push(Reverse(item))
    }

    pub fn pop(&mut self) -> Option<T> {
        self.heap.pop().map(|Reverse(x)| x)
    }
}

impl<T> Default for MinHeap<T>
where
    T: Ord,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<T, const N: usize> From<[T; N]> for MinHeap<T>
where
    T: Ord,
{
    fn from(value: [T; N]) -> Self {
        Self {
            heap: BinaryHeap::from(value.map(|x| Reverse(x))),
        }
    }
}

impl<T: Ord> FromIterator<T> for MinHeap<T>
where
    T: Ord,
{
    fn from_iter<U: IntoIterator<Item = T>>(iter: U) -> Self {
        Self {
            heap: BinaryHeap::from_iter(iter.into_iter().map(|x| Reverse(x))),
        }
    }
}
