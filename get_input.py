#!/usr/bin/env python3

import datetime
import yaml
import requests
import sys


def load_config():
    with open(".env.yaml", "r") as f:
        js = yaml.load(f, Loader=yaml.Loader)
        return js["session"], js["year"]


def get_input(session, year, day):
    return requests.get(
        f"https://adventofcode.com/{year}/day/{day}/input",
        cookies={"session": session},
        headers={
            "User-Agent": "{repo} by {mail}".format(
                repo="gitlab.com/mfocko/advent-of-code-2022",
                mail="me@mfocko.xyz",
            )
        },
    ).content.decode("utf-8")


def main():
    day = datetime.datetime.now().day
    if len(sys.argv) == 2:
        day = sys.argv[1]

    session, year = load_config()
    problem_input = get_input(session, year, day)

    with open(f"./inputs/day{day:>02}.txt", "w") as f:
        f.write(problem_input)


if __name__ == "__main__":
    main()
